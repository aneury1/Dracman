

/*
 * GraphicsManager.cpp
 *
 *  Created on: Nov 12, 2015
 *      Author: aperez
 */


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

#include"GraphicsManager.hpp"
#include"Device.hpp"

#define DEBUG_ENABLE 1
GraphicsManager* GraphicsManager::m_instance  = 0;


void GraphicsManager::LoadImg(Device *device, string src, string id)
{
     /*
      * Cosas por hacer agregar la solucion para que los graficos se carguen desde la carpeta ESPECIFICA...
      * */

    SDL_Surface *surface = SDL_LoadBMP(src.c_str());
    if(surface == NULL)
    {
    	SDL_Log("Error : [%s], Function [%s], Line [%d]", SDL_GetError(), __FUNCTION__, __LINE__);
        device->Running = false;
        return;
    }

    SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, device->transparent_color.r,
    		                                      device->transparent_color.g ,device->transparent_color.b)
    		                                      );
    SDL_Texture *texture = SDL_CreateTextureFromSurface(device->render, surface);
    if(texture == NULL)
    {
    	SDL_Log("Error : [%s], Function [%s], Line [%d]", SDL_GetError(), __FUNCTION__, __LINE__);
        device->Running = false;
        return;
    }

    m_img[id] = texture;
    SDL_FreeSurface(surface);
}


void GraphicsManager::DrawImg(Device *device,string id, int x, int y, int w,int h,SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
//Set rendering space and render to screen
   SDL_Rect renderQuad = { x, y, w, h };

   //Set clip rendering dimensions
   if( clip != NULL )
   {
       renderQuad.w = clip->w;
       renderQuad.h = clip->h;
   }
   //Render to screen
   SDL_RenderCopyEx(device->render, this->m_img[id], clip, &renderQuad, angle, center, flip );
}

void GraphicsManager::DrawImg(Device *device,string id,
		                       int x, int y,
							   int w,int h,
							   int col, int row,
							   int hsize, int vsize, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
//Set rendering space and render to screen
   SDL_Rect renderQuad = { x, y, w, h };
   SDL_Rect clip       = {row,col, hsize,vsize};

   //Render to screen
   SDL_RenderCopyEx(device->render, this->m_img[id], &clip, &renderQuad, angle, center, flip );
}


void GraphicsManager::DrawImg(Device *device, string id, SDL_Rect clipset,SDL_Rect offset )
{
	 SDL_RenderCopy(device->render, m_img[id], &clipset,&offset);
}








void GraphicsManager::DrawImg(Device *device, string id,  int x, int y, int w, int h, int col, int row, int hsize, int vsize)
{
	 SDL_Rect offset = {x,y,w,h};
	 SDL_Rect clipset = {col, row, hsize, vsize};
	 SDL_RenderCopy(device->render, m_img[id], &clipset,&offset);
}

void GraphicsManager::DeleteImage(Device *device, string id)
{
   SDL_DestroyTexture(m_img[id]);
   m_img.erase(id);
}

#ifdef UNUSED
void GraphicsManager::DrawImg(Device *device, Object object)
{
	SDL_RenderCopy(device->render, m_img[object.id], &object.clipset,&object.offset);
}
#endif



void GraphicsManager::LoadFont(Device *device, string src, int ptsize )
{
#ifdef DEBUG_ENABLED
	SDL_Log("--LoadFont--");
#endif
	 device->main_font=  TTF_OpenFont(src.c_str(),ptsize);
     if(device->main_font == NULL)
     {
    	 SDL_Log("Error :Function :[%s] This line : [%d] ,Error Type:[%s]",__FUNCTION__,__LINE__, TTF_GetError());
     }
     else
     {
    	// device->font_name1 =src;
     }
}




void GraphicsManager::CreateText(Device *device, string id, string text, SDL_Color color)
{
#ifdef DEBUG_ENABLED
	SDL_Log("--CreateText--");
#endif
	SDL_Surface *surface = NULL;
	SDL_Texture *texture = NULL;
	if(device->main_font != NULL)
	{
		 surface = TTF_RenderText_Solid(device->main_font , text.c_str(),color);
	}
	else
	{
		// surface = TTF_RenderText_Solid(device->aux_font  , text.c_str() ,color);
	}

	if(surface == NULL)
	{
		SDL_Log("Error [%s]", TTF_GetError());
	}
	else
	{
	    texture = SDL_CreateTextureFromSurface(device->render , surface);
	}

	if(texture == NULL)
	{
		SDL_Log("ERror");
	}
	else
	{
		this->m_img[id] = texture;
	}

}






void GraphicsManager::RenderText(Device *device, string text, int x, int y ,int w, int h)
{
	SDL_Color color = {255,0,0,255};
    SDL_Surface *surface = TTF_RenderText_Solid(device->main_font , text.c_str(),color);
    SDL_Texture *texture = NULL;
	if(device->main_font != NULL)
		{
			 if(surface != NULL)
			 {
#ifdef DEBUG_ENABLED
				     SDL_Log("=======================================================");
				     SDL_Log("texto : %s , %d, %d, %d \n", text.c_str(), color.r,color.b,color.g);
				 	// SDL_Log("Error ttf :[%d]",TTF_GetError());
				     SDL_Log("=======================================================");
#endif
		     }
			 else
		     {
		    	 SDL_Log("no , el surface es nulo");
		     }
		}
	else
       	{
		     SDL_Log("there is not font file");
	     }
	texture =  SDL_CreateTextureFromSurface(device->render, surface);


	SDL_Rect offset ={x,y,w,h};
	if(texture != NULL)
	{
		SDL_RenderCopy(device->render, texture, NULL, &offset);
	    SDL_DestroyTexture(texture);
	    texture = NULL;
	}
	else
		SDL_Log("error con la bendita textura");

}




void GraphicsManager::SetViewPort(Device *device, SDL_Rect rect)
{
	 SDL_RenderSetViewport( device->render, &rect );
	 SDL_RenderPresent(device->render);
}

 void GraphicsManager::DrawImg(Device *device, string id, int x,int y,int w,int h )
{
  SDL_Rect offset = {x,y,w,h};
  SDL_RenderCopy(device->render, m_img[id], NULL, &offset);
}


void GraphicsManager::ModuleColorImage(Device *device, string id, SDL_Color color)
{
    SDL_SetTextureColorMod(this->m_img[id],color.r, color.g,color.b);
}
void GraphicsManager::ModuleColorImage(Device *device, string id, Uint8 r,Uint8 g,Uint8 b )
{
	SDL_SetTextureColorMod(this->m_img[id],r, g,b);
}


void GraphicsManager::setBlendMode( SDL_BlendMode blending,string id )
{
	SDL_SetTextureBlendMode( this->m_img[id], blending );
}

void GraphicsManager::setAlpha( Uint8 alpha,string id )
{
    //Modulate texture alpha
    SDL_SetTextureAlphaMod( this->m_img[id], alpha );
}







