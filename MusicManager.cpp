/*
 * MusicManager.cpp
 *
 *  Created on: 2 de ene. de 2016
 *      Author: aneury
 */



#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif

#include<iostream>
#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;
#include"MusicManager.hpp"


 MusicManager* MusicManager::m_instance = 0;


 void MusicManager::LoadMusic(Device *device, const char *src, string id)
 {


         m_music[id] = Mix_LoadMUS(src);
         SDL_Log("CArgado");

 }

void MusicManager::PlayMusic(Device *device, string id , int vol)
{
	 SDL_Log("reproduciendo");
	 if( Mix_PlayingMusic() == 0 )
	   {
	                                //Play the music
	     Mix_PlayMusic( m_music[id], -1 );
	   }
	                            //If music is being played
	    else
	   {
	                                //If the music is paused
	     if( Mix_PausedMusic() == 1 )
	         {
	                                    //Resume the music
	           Mix_ResumeMusic();
	         }
	                                //If the music is playing
	           else
	         {
	                                    //Pause the music
	           Mix_PauseMusic();
	         }
	   }

}


void MusicManager::LoadWav(Device *device, const char *src, string id)
{
	 Mix_Chunk * temp = Mix_LoadWAV(src);




       if( temp  == NULL)
       {
    	  SDL_Log("Error cargando ");
    	device->Running = false;

       }else
       {
    	   m_sound[id] = temp;
       }

}




void MusicManager::PlayWav(Device *device ,string id)
{
	 Mix_PlayChannel( -1, m_sound[id], 1 );
}
void MusicManager::SetVolume(unsigned int l)
{


}
