/*
 * OptionState.cpp
 *
 *  Created on: 11 de ene. de 2016
 *      Author: aneury
 */




/*
 * OptionState.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */







#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif

#include<iostream>
#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

///project include here
#include"GameState.hpp"
#include"InputHandler.hpp"
#include"OptionState.hpp"
#include"GraphicsManager.hpp"



OptionState::OptionState()
{


}



void OptionState::init(Device *device)
{
	puntero_intro = new Object(device,device->window_param.w /2 - 132,device->window_param.h/2,32,32,UP,"puntero_init");
	GraphicsManager::Graphics()->LoadImg(device, "optionMenuBorder.bmp","optionBorder");
	GraphicsManager::Graphics()->LoadImg(device, "mouse_pointer.bmp","m1");

}



OptionState::~OptionState()
{


}
void OptionState::Update(Device *device)
{
	static bool inicio = true;





	 if(InputHandler::InputManager()->IsPressed(SDLK_UP))
	 {
	    if(inicio == true)
		   inicio = false;
	    else
	       inicio = true;

		if(inicio == true)
	    	puntero_intro->setYPosition(device->window_param.h/2);
	    else
	    	puntero_intro->setYPosition(device->window_param.h/2+ 60);
	 }
	 if(InputHandler::InputManager()->IsPressed(SDLK_DOWN))
	  {
		 if(inicio == true)
			   inicio = false;
		    else
		       inicio = true;

			if(inicio == true)
		    	puntero_intro->setYPosition(device->window_param.h/2);
		    else
		    	puntero_intro->setYPosition(device->window_param.h/2+ 60);


	  }

}
void OptionState::Draw(Device *device)
{
       SDL_Rect m1 = { 0,0,64,52};
	   GraphicsManager::Graphics()->DrawImg(device,"optionBorder",0,0,device->window_param.w,device->window_param.h);
	   GraphicsManager::Graphics()->DrawImg(device,"m1",InputHandler::InputManager()->getMouse(),m1);

}
