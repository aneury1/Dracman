/*
 * Camera.hpp
 *
 *  Created on: 2 de ene. de 2016
 *      Author: aneury
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_




#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;




class Camera
{
	static int SuperScroll;
    static int limit;

    SDL_Rect camera;
    SDL_Rect camera_clipset;
    SDL_Rect initial_pos;



public:

    Camera( int x, int y,int w,int h);

    void Reset();

    void SetCamera(int x, int y, int w, int h);
    void SetCameraX(int xf);
    void SetCameraY(int yf);

    int getCameraX()const{return camera.x;}
    int getCameraY()const{return camera.y;}

    void SetCameraOffset(SDL_Rect r1);
    void SetCameraClipset(SDL_Rect r1);
    SDL_Rect getCameraOffset();
    SDL_Rect getCameraClipset();




    void initScroll(int xvel = 23);
    bool isInLimit();



};






#endif /* CAMERA_HPP_ */
