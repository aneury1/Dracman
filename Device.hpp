/*
 * Device.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#ifndef DEVICE_HPP_
#define DEVICE_HPP_
#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

typedef struct
{
   SDL_Window        *window;
   SDL_Renderer      *render;
   SDL_Rect           window_param;
   SDL_Rect           touch_offset;
   SDL_Event          event;
   bool               Running;
   stringstream       title;
   stringstream       configuration_file;
   unsigned int       current_state;
   SDL_Color          transparent_color;
   TTF_Font*          main_font;
   int test;
   /* */
   unsigned int menu_type;

}Device;

#define VERTICAL_MENU     0
#define HORIZONTAL_MENU   1
#define CIRCULAR_MENU     2

#endif /* DEVICE_HPP_ */
