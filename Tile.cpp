/*
 * Tile.cpp
 *
 *  Created on: 3 de ene. de 2016
 *      Author: aneury
 */


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;


#include"tile.hpp"


Tile::Tile(int x, int y, char type)
{
  offset.x = x;
  offset.y = y;
  offset.w = TILE_SIZE;
  offset.h = TILE_SIZE;
  tiletype = type;
}
SDL_Rect Tile::getOffset()
{
   return offset;
}
SDL_Rect Tile::getClipset()
{
   return clipset;
}
int Tile::getX()
{
    return offset.x;
}
int Tile::getY()
{
   return offset.y;
}
int Tile::getTileSize()
{
   return TILE_SIZE;
}

char Tile::getType()
{
   return 	tiletype;
}

#if 0
/*
 * Main.cpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */


#ifndef ANDROID_DEV

#include<map>
using namespace std;
#endif

#include<SDL2/SDL.h>
#include<string>
#include<cstdlib>
#include<iostream>
using namespace std;

#define W        960
#define H        640
#define MAXFILAS 21
#define MAXCOLS  31

#define TILESIZE 32
#define LEFT     0
#define RIGHT    1
#define UP       2
#define DOWN     3
#define NONE     4

static int Pacman_dirx = 32;
static int Pacman_diry = 32;




char mapa1[MAXFILAS][MAXCOLS]={
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
 "X  o |o o o XXXXX o o o| o  X",
 "X XXX XXXXX XXXXX XXXXX XXX X",
 "XoXXX XXXXX XXXXX XXXXX XXXoX",
 "X      o|o   o o   o|o      X",
 "XoXXXoXX XXXXXXXXXXX XXoXXXoX",
 "X    |XX    |XXX|    XX     X",
 "XoXXXoXXXXXX XXX XXXXXXoXXXoX",
 "X XXXoXX ooo|ooo|ooo XXoXXX X",
 " o   |XX XXXXXXXXXXX XX|   o ",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXXoXX oo |ooo|ooo XXoXXXoX",
 "X XXXoXXXXXX XXX XXXXXXoXXX X",
 "X     XX     XXX     XX     X",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXX| o| o o o o o |o |XXXoX",
 "X XXXoXXXX XXXXXXXX XXX XXX X",
 "XoXXXoXXXX          XXX XXXoX",
 "X  o |o o  XXXXXXXX o o| o  X",
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
};

SDL_Color transaparent ={0,0,0,0 };

SDL_Window   *window;
SDL_Renderer *render;
SDL_Rect      windowCoord;



SDL_Texture  *block1;
SDL_Texture  *block2;
SDL_Texture  *enemigos;
SDL_Texture  *personaje;
SDL_Texture  *moneda;
SDL_Texture  *muerte;
SDL_Texture  *rapple;
SDL_Texture  *gapple;
SDL_Texture  *hammer;




SDL_Texture *arrows1;
SDL_Texture *arrows2;
SDL_Texture *arrows3;
SDL_Texture *arrows4;
SDL_Rect     a1Rect;
SDL_Rect     a2Rect;
SDL_Rect     a3Rect;
SDL_Rect     a4Rect;



#define ISQUIERDA 0
#define DERECHA   1
#define ARRIBA    2
#define ABAJO     3
#define NINGUNO   4



static int dir = NINGUNO;




SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic);
void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz);
bool LoadResource();
void DrawMap(SDL_Renderer *render);
void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE = NONE);
bool checkCollision( SDL_Rect a, SDL_Rect b );
void CanMove(int &x,int &y,int current = 0);






class EventHandler
{
  EventHandler(){}
  static EventHandler* m_instance;

  map<SDL_Keycode, bool>m_held;
  map<SDL_Keycode, bool>m_pressed;
  map<SDL_Keycode, bool>m_released;
  SDL_Rect touch;
public:

  static EventHandler* Input()
  {
	  if(m_instance == NULL)
	    m_instance = new EventHandler();
	  return m_instance;
  }

  void KeyDown(SDL_Event event);
  void KeyUp(SDL_Event event);
  void Touch(SDL_Event event);
  bool Pressed(SDL_Keycode code);
  bool released(SDL_Keycode code);
  bool held(SDL_Keycode code);
  SDL_Rect getTouch();
  void Clear();


};



 EventHandler* EventHandler::m_instance = 0;

 void EventHandler::KeyDown(SDL_Event event)
 {
	 m_held[event.key.keysym.sym]     = true;
	 m_pressed[event.key.keysym.sym] = true;
 }

void EventHandler::KeyUp(SDL_Event event)
{
  m_released[event.key.keysym.sym]= true;
}

void EventHandler::Touch(SDL_Event event)
{
	touch.x = event.tfinger.x;
	touch.y = event.tfinger.y;
    touch.w = TILESIZE;
    touch.h = TILESIZE;
}


bool EventHandler::Pressed(SDL_Keycode code)
{
   return m_pressed[code];
}
bool EventHandler::released(SDL_Keycode code)
{
    return m_released[code];
}
bool EventHandler::held(SDL_Keycode code)
{
    return m_held[code];
}
SDL_Rect EventHandler::getTouch()
{
   return touch;
}

void EventHandler::Clear()
  {
	 m_held.clear();
	 m_pressed.clear();
	 m_released.clear();
  }







int main(int argc, char *argv[])
{

	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_CreateWindowAndRenderer(W,H, SDL_WINDOW_BORDERLESS| SDL_WINDOW_SHOWN, &window, &render);
	bool run = true;
	SDL_Event event;

	if(!LoadResource())
		cout<<"Error Cargando bmps";



	while(run)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			    case SDL_KEYDOWN:
			    	EventHandler::Input()->KeyDown(event);
			     break;
			    case SDL_KEYUP:
			    	EventHandler::Input()->KeyUp(event);
			     break;
			    case SDL_FINGERDOWN:
			    	EventHandler::Input()->Touch(event);
			     break;
			    case SDL_FINGERUP:
			    	EventHandler::Input()->Clear();
			     break;
			}
		}



		if(EventHandler::Input()->Pressed(SDLK_q))
			  run = false;
		else if(EventHandler::Input()->Pressed(SDLK_RIGHT))
			          dir = DERECHA;
		else if(EventHandler::Input()->Pressed(SDLK_LEFT))
					  dir = ISQUIERDA;
		else if(EventHandler::Input()->Pressed(SDLK_UP))
					  dir = ARRIBA;
		else if(EventHandler::Input()->Pressed(SDLK_DOWN))
					  dir = ABAJO;
		else
			          dir = NINGUNO;




       switch(dir)
       {
         case ARRIBA:
        	 Pacman_diry -=32;



    	   break;
         case ABAJO:
        	 Pacman_diry +=32;

           break;
         case DERECHA:
        	 Pacman_dirx +=32;


        	 break;
         case ISQUIERDA:
        	 Pacman_dirx -=32;

             break;

       }

















		SDL_RenderClear(render);
		SDL_SetRenderDrawColor(render, 0xa,0xa,0xa,0xff);
		DrawMap(render);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,dir);

		SDL_RenderPresent(render);
		SDL_Delay(60);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,NINGUNO);

		SDL_RenderPresent(render);
		EventHandler::Input()->Clear();
	}


 SDL_DestroyWindow(window);
 SDL_DestroyRenderer(render);
 return 0;
}













void DrawMap(SDL_Renderer *render)
{

    for(int row = 0 ;row <  MAXFILAS; row++)
    {
    	for(int col= 0; col < MAXCOLS; col++)
    	{

#ifndef ANDROID_DEV
    		if(mapa1[row][col] == 'X')
    		{
    			SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    			DibujarObjecto(render,block1, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		}
    		if(mapa1[row][col] == 'o')
    		  {
    		    SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    DibujarObjecto(render,moneda, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		  }
    		if(mapa1[row][col] == 'a')
    		   {
    		    		    SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    		    DibujarObjecto(render,moneda, col *32,row *32   ,30,30, 0, 0, 32, 32);
        		  }




#else  ///desktop
    		if(mapa1[row][col] == 'X')
    		  {
    		    			SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    			DibujarObjecto(render,block1, row *32,col *32   ,30,30, 0, 0, 32, 32);
    		   }
    		if(mapa1[row][col] == 'o')
    		    {
    		    	 SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    	DibujarObjecto(render,moneda,row *32 , col *32  ,30,30, 0, 0, 32, 32);
    		    }
#endif

    	}
    }

}








SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic)
{
  SDL_Texture *ret = NULL;
  SDL_Surface *lsf = SDL_LoadBMP(src);
  SDL_Log("Imagen a cargar [%s]", src);

  int t = SDL_SetColorKey(lsf, SDL_TRUE, SDL_MapRGB(lsf->format, color_magic.r, color_magic.g, color_magic.b));
  SDL_Log("SDL_SetColorKey devolvio [%d]", t );

  ret =  SDL_CreateTextureFromSurface(render, lsf);
  SDL_Log("SDL_CreateTextureFromSurface devolvio [%d]", ret);


  return ret;
}




void CanMove(int &x,int &y,int current = 0)
{


}





bool LoadResource()
{
   bool fRet = false;
   block1    = loadTexture(render, "roca1.bmp",transaparent) ;
   block2    = loadTexture(render, "roca2.bmp",transaparent) ;
   enemigos  = loadTexture(render, "enemigo.bmp",transaparent) ;
   personaje = loadTexture(render, "pacman.bmp",transaparent) ;
   moneda    = loadTexture(render, "moneda.bmp",transaparent) ;
   muerte    = loadTexture(render, "muerte.bmp", transaparent);
   arrows1   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows2   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows3   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows4   = loadTexture(render, "arrow1.bmp", transaparent);



   if(block1   != NULL &&
     block2    != NULL &&
	 enemigos  != NULL &&
	 personaje != NULL &&
	 moneda    != NULL &&
	 arrows1   != NULL &&
     arrows2   != NULL &&
	 arrows3   != NULL &&
	 arrows4   != NULL)
	   fRet= true;


   return fRet;

}




void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz)
{
   SDL_Rect offset  = {x,y,w,h};
   SDL_Rect clipset = {row, col, rz,cz};
   SDL_RenderCopy(render, t1, &clipset, &offset);
}





void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE)
{
     SDL_Rect offset = {x,y,TILESIZE,TILESIZE};
     SDL_Rect clipset= {33*FACE, 0, TILESIZE,TILESIZE};
     SDL_RenderCopy(render, t, &clipset, &offset);


}






bool checkCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    //If any of the sides from A are outside of B
        if( bottomA <= topB )
        {
            return false;
        }

        if( topA >= bottomB )
        {
            return false;
        }

        if( rightA <= leftB )
        {
            return false;
        }

        if( leftA >= rightB )
        {
            return false;
        }

        //If none of the sides from A are outside B
        return true;
    }



#endif

#ifdef VERSION2PACMANRELEASE

/*
 * Main.cpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */


#ifndef ANDROID_DEV

#include<map>
using namespace std;
#endif

#include<SDL2/SDL.h>
#include<string>
#include<cstdlib>
#include<iostream>
using namespace std;

#define W        960
#define H        640
#define MAXFILAS 21
#define MAXCOLS  31

#define TILESIZE 32
#define LEFT     0
#define RIGHT    1
#define UP       2
#define DOWN     3
#define NONE     4

static int Pacman_dirx = 32;
static int Pacman_diry = 32;
static unsigned int score = 0;

/*
 *   o es para moneda una vez comido se reemplazan con 0
 *   A es para manzana verde  una vez comido se reemplazan con 9
 *   a es para manzana roja   una vez comido se reemplazan con 8
 *   h es para martillo una vez comido se reemplazan con 8
 * */

char mapa1[MAXFILAS][MAXCOLS]={
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
 "X  o |o o o XXXXX o o o| o  X",
 "X XXX XXXXX XXXXX XXXXX XXX X",
 "XoXXX XXXXX XXXXX XXXXX XXXoX",
 "X      o|o   o o   o|o  A   X",
 "XoXXXoXX XXXXXXXXXXX XXoXXXoX",
 "X    |XX    |XXX|    XX     X",
 "XoXXXoXXXXXX XXX XXXXXXoXXXoX",
 "X XXXoXX ooo|ooo|ooo XXoXXX X",
 " o   |XX XXXXXXXXXXX XX|   o ",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXXoXX oo |ooo|ooo XXoXXXoX",
 "X XXXoXXXXXX XXX XXXXXXoXXX X",
 "X     XX     XXX a   XX     X",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXX| o| o o o o o |o |XXXoX",
 "X XXXoXXXX XXXXXXXX XXX XXX X",
 "XoXXXoXXXX          XXX XXXoX",
 "X  o |o o  XXXXXXXX o o| o  X",
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
};

SDL_Color transaparent ={0,0,0,0 };

SDL_Window   *window;
SDL_Renderer *render;
SDL_Rect      windowCoord;



SDL_Texture  *block1;
SDL_Texture  *block2;
SDL_Texture  *enemigos;
SDL_Texture  *personaje;
SDL_Texture  *moneda;
SDL_Texture  *muerte;
SDL_Texture  *rapple;
SDL_Texture  *gapple;
SDL_Texture  *hammer;




SDL_Texture *arrows1;
SDL_Texture *arrows2;
SDL_Texture *arrows3;
SDL_Texture *arrows4;
SDL_Rect     a1Rect;
SDL_Rect     a2Rect;
SDL_Rect     a3Rect;
SDL_Rect     a4Rect;



#define ISQUIERDA 0
#define DERECHA   1
#define ARRIBA    2
#define ABAJO     3
#define NINGUNO   4



static int dir = NINGUNO;




SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic);
void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz);
bool LoadResource();
void DrawMap(SDL_Renderer *render);
void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE = NONE);
bool checkCollision( SDL_Rect a, SDL_Rect b );
void CanMove(int &x,int &y,int current = 0);






class EventHandler
{
  EventHandler(){}
  static EventHandler* m_instance;

  map<SDL_Keycode, bool>m_held;
  map<SDL_Keycode, bool>m_pressed;
  map<SDL_Keycode, bool>m_released;
  SDL_Rect touch;
public:

  static EventHandler* Input()
  {
	  if(m_instance == NULL)
	    m_instance = new EventHandler();
	  return m_instance;
  }

  void KeyDown(SDL_Event event);
  void KeyUp(SDL_Event event);
  void Touch(SDL_Event event);
  bool Pressed(SDL_Keycode code);
  bool released(SDL_Keycode code);
  bool held(SDL_Keycode code);
  SDL_Rect getTouch();
  void Clear();


};



 EventHandler* EventHandler::m_instance = 0;

 void EventHandler::KeyDown(SDL_Event event)
 {
	 m_held[event.key.keysym.sym]     = true;
	 m_pressed[event.key.keysym.sym] = true;
 }

void EventHandler::KeyUp(SDL_Event event)
{
  m_released[event.key.keysym.sym]= true;
}

void EventHandler::Touch(SDL_Event event)
{
	touch.x = event.tfinger.x;
	touch.y = event.tfinger.y;
    touch.w = TILESIZE;
    touch.h = TILESIZE;
}


bool EventHandler::Pressed(SDL_Keycode code)
{
   return m_pressed[code];
}
bool EventHandler::released(SDL_Keycode code)
{
    return m_released[code];
}
bool EventHandler::held(SDL_Keycode code)
{
    return m_held[code];
}
SDL_Rect EventHandler::getTouch()
{
   return touch;
}

void EventHandler::Clear()
  {
	 m_held.clear();
	 m_pressed.clear();
	 m_released.clear();
  }







int main(int argc, char *argv[])
{

	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_CreateWindowAndRenderer(W,H, SDL_WINDOW_BORDERLESS| SDL_WINDOW_SHOWN, &window, &render);
	bool run = true;
	SDL_Event event;

	if(!LoadResource())
		cout<<"Error Cargando bmps";



	while(run)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			    case SDL_KEYDOWN:
			    	EventHandler::Input()->KeyDown(event);
			     break;
			    case SDL_KEYUP:
			    	EventHandler::Input()->KeyUp(event);
			     break;
			    case SDL_FINGERDOWN:
			    	EventHandler::Input()->Touch(event);
			     break;
			    case SDL_FINGERUP:
			    	EventHandler::Input()->Clear();
			     break;
			}
		}



		if(EventHandler::Input()->Pressed(SDLK_q))
			  run = false;
		else if(EventHandler::Input()->Pressed(SDLK_RIGHT))
			          dir = DERECHA;
		else if(EventHandler::Input()->Pressed(SDLK_LEFT))
					  dir = ISQUIERDA;
		else if(EventHandler::Input()->Pressed(SDLK_UP))
					  dir = ARRIBA;
		else if(EventHandler::Input()->Pressed(SDLK_DOWN))
					  dir = ABAJO;
		else
			          dir = NINGUNO;




       switch(dir)
       {
         case ARRIBA:

        	 if(mapa1[(Pacman_diry-32)/32][Pacman_dirx/32] != 'X')
                    Pacman_diry -=32;

        	 break;
         case ABAJO:
        	 if(mapa1[(Pacman_diry+32)/32][Pacman_dirx/32] != 'X')
        	 Pacman_diry +=32;

           break;
         case DERECHA:
        	 if(mapa1[Pacman_diry/32][((Pacman_dirx+32)/32)] != 'X')
        	 Pacman_dirx +=32;


        	 break;
         case ISQUIERDA:


        	 if(mapa1[Pacman_diry/32][((Pacman_dirx-32)/32)] != 'X')
        	                     Pacman_dirx -=32;
        	 ///Pacman_dirx -=32;

             break;

       }

















		SDL_RenderClear(render);
		SDL_SetRenderDrawColor(render, 0xa,0xa,0xa,0xff);
		DrawMap(render);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,dir);

		SDL_RenderPresent(render);
		SDL_Delay(60);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,NINGUNO);

		SDL_RenderPresent(render);
		EventHandler::Input()->Clear();
	    SDL_Log("Score : [%d]", score);
	}


 SDL_DestroyWindow(window);
 SDL_DestroyRenderer(render);
 return 0;
}













void DrawMap(SDL_Renderer *render)
{

    for(int row = 0 ;row <  MAXFILAS; row++)
    {
    	for(int col= 0; col < MAXCOLS; col++)
    	{

#ifndef ANDROID_DEV
    		if(mapa1[row][col] == 'X')
    		{
    			//SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    			DibujarObjecto(render,block1, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		}
    		if(mapa1[row][col] == 'o')
    		  {
    		    //SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    DibujarObjecto(render,moneda, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		     if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		     {
    		    	 score += 10;
    		    	 SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    	 mapa1[row][col] = '0';
    		     }


    		  }
    		if(mapa1[row][col] == 'a')
    		   {
    		    		  //  SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    		    DibujarObjecto(render,rapple, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		    	 if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		    		{
    		    		     SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    		      mapa1[row][col] = '8';
    		    	       score +=100;
    		    		}
        	   }
    		if(mapa1[row][col] == 'A')
    		    {
    		    		    		  //  SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    	DibujarObjecto(render,gapple, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		    	 if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		    		     {
    		    		     SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    		    mapa1[row][col] = '9';
    		    	         score+=1000;
    		    		     }
    		     }



#else  ///desktop
    		if(mapa1[row][col] == 'X')
    		  {
    		    			SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    			DibujarObjecto(render,block1, row *32,col *32   ,30,30, 0, 0, 32, 32);
    		   }
    		if(mapa1[row][col] == 'o')
    		    {
    		    	 SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    	DibujarObjecto(render,moneda,row *32 , col *32  ,30,30, 0, 0, 32, 32);
    		    }
#endif

    	}
    }

}








SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic)
{
  SDL_Texture *ret = NULL;
  SDL_Surface *lsf = SDL_LoadBMP(src);
  SDL_Log("Imagen a cargar [%s]", src);

  int t = SDL_SetColorKey(lsf, SDL_TRUE, SDL_MapRGB(lsf->format, color_magic.r, color_magic.g, color_magic.b));
  SDL_Log("SDL_SetColorKey devolvio [%d]", t );

  ret =  SDL_CreateTextureFromSurface(render, lsf);
  SDL_Log("SDL_CreateTextureFromSurface devolvio [%d]", ret);


  return ret;
}




void CanMove(int &x,int &y,int current)
{


}





bool LoadResource()
{
   bool fRet = false;
   block1    = loadTexture(render, "roca1.bmp",transaparent) ;
   block2    = loadTexture(render, "roca2.bmp",transaparent) ;
   enemigos  = loadTexture(render, "enemigo.bmp",transaparent) ;
   personaje = loadTexture(render, "pacman.bmp",transaparent) ;
   moneda    = loadTexture(render, "moneda.bmp",transaparent) ;
   muerte    = loadTexture(render, "muerte.bmp", transaparent);
   arrows1   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows2   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows3   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows4   = loadTexture(render, "arrow1.bmp", transaparent);
   rapple    = loadTexture(render, "rapple.bmp", transaparent);
   gapple    = loadTexture(render, "gapple.bmp", transaparent);

   if(block1   != NULL &&
     block2    != NULL &&
	 enemigos  != NULL &&
	 personaje != NULL &&
	 moneda    != NULL &&
	 arrows1   != NULL &&
     arrows2   != NULL &&
	 arrows3   != NULL &&
	 arrows4   != NULL &&
	 rapple    != NULL &&
	 gapple    != NULL)
	   fRet= true;


   return fRet;

}




void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz)
{
   SDL_Rect offset  = {x,y,w,h};
   SDL_Rect clipset = {row, col, rz,cz};
   SDL_RenderCopy(render, t1, &clipset, &offset);
}





void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE)
{
     SDL_Rect offset = {x,y,TILESIZE,TILESIZE};
     SDL_Rect clipset= {33*FACE, 0, TILESIZE,TILESIZE};
     SDL_RenderCopy(render, t, &clipset, &offset);


}






bool checkCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    //If any of the sides from A are outside of B
        if( bottomA <= topB )
        {
            return false;
        }

        if( topA >= bottomB )
        {
            return false;
        }

        if( rightA <= leftB )
        {
            return false;
        }

        if( leftA >= rightB )
        {
            return false;
        }

        //If none of the sides from A are outside B
        return true;
    }



#endif
#ifdef VERSION3DRACMAM

/*
 * Main.cpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */


#ifndef ANDROID_DEV

#include<map>
using namespace std;
#endif





#include<SDL2/SDL.h>
#include<string>
#include<cstdlib>
#include<iostream>
using namespace std;

#define W        960
#define H        640
#define MAXFILAS 21
#define MAXCOLS  31

#define TILESIZE 32
#define LEFT     0
#define RIGHT    1
#define UP       2
#define DOWN     3
#define NONE     4

static int Pacman_dirx = 32;
static int Pacman_diry = 32;
static unsigned int score = 0;

/*
 *   o es para moneda una vez comido se reemplazan con 0
 *   A es para manzana verde  una vez comido se reemplazan con 9
 *   a es para manzana roja   una vez comido se reemplazan con 8
 *   h es para martillo una vez comido se reemplazan con 8
 * */

char mapa1[MAXFILAS][MAXCOLS]={
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
 "X  o |o o o XXXXX o o o| o  X",
 "X XXX XXXXX XXXXX XXXXX XXX X",
 "XoXXX XXXXX XXXXX XXXXX XXXoX",
 "X      o|o   o o   o|o  A   X",
 "XoXXXoXX XXXXXXXXXXX XXoXXXoX",
 "X    |XX    |XXX|    XX     X",
 "XoXXXoXXXXXX XXX XXXXXXoXXXoX",
 "X XXXoXX ooo|ooo|ooo XXoXXX X",
 "Io   |XX XXXXXXXXXXX XX|   oD",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXXoXX oo |ooo|ooo XXoXXXoX",
 "X XXXoXXXXXX XXX XXXXXXoXXX X",
 "X     XX     XXX a   XX     X",
 "X XXXoXX XXXXXXXXXXX XXoXXX X",
 "XoXXX| o| o o o o o |o |XXXoX",
 "X XXXoXXXX XXXXXXXX XXX XXX X",
 "XoXXXoXXXX          XXX XXXoX",
 "X  o |o o  XXXXXXXX o o| o  X",
 "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
};

///ESTAS VARIABLES ESTAN PARA LOS ATAJOS DENTRO DEL MAPA I y D PAra idetificarlos en el mapa
 static int atajo_isquierdaX = 0;
 static int atajo_isquierdaY = 0;
 static int atajo_derechaX   = 0;
 static int atajo_derechaY   = 0;




SDL_Color transaparent ={0,0,0,0 };

SDL_Window   *window;
SDL_Renderer *render;
SDL_Rect      windowCoord;



SDL_Texture  *block1;
SDL_Texture  *block2;
SDL_Texture  *enemigos;
SDL_Texture  *personaje;
SDL_Texture  *moneda;
SDL_Texture  *muerte;
SDL_Texture  *rapple;
SDL_Texture  *gapple;
SDL_Texture  *hammer;




SDL_Texture *arrows1;
SDL_Texture *arrows2;
SDL_Texture *arrows3;
SDL_Texture *arrows4;
SDL_Rect     a1Rect;
SDL_Rect     a2Rect;
SDL_Rect     a3Rect;
SDL_Rect     a4Rect;



#define ISQUIERDA 0
#define DERECHA   1
#define ARRIBA    2
#define ABAJO     3
#define NINGUNO   4



static int dir = NINGUNO;




SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic);
void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz);
bool LoadResource();
void DrawMap(SDL_Renderer *render);
void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE = NONE);
bool checkCollision( SDL_Rect a, SDL_Rect b );
bool GameOver(int current =0);


//not longer necesary
void CanMove(int &x,int &y,int current = 0);






class EventHandler
{
  EventHandler(){}
  static EventHandler* m_instance;

  map<SDL_Keycode, bool>m_held;
  map<SDL_Keycode, bool>m_pressed;
  map<SDL_Keycode, bool>m_released;
  SDL_Rect touch;
public:

  static EventHandler* Input()
  {
	  if(m_instance == NULL)
	    m_instance = new EventHandler();
	  return m_instance;
  }

  void KeyDown(SDL_Event event);
  void KeyUp(SDL_Event event);
  void Touch(SDL_Event event);
  bool Pressed(SDL_Keycode code);
  bool released(SDL_Keycode code);
  bool held(SDL_Keycode code);
  SDL_Rect getTouch();
  void Clear();


};



 EventHandler* EventHandler::m_instance = 0;

 void EventHandler::KeyDown(SDL_Event event)
 {
	 m_held[event.key.keysym.sym]     = true;
	 m_pressed[event.key.keysym.sym] = true;
 }

void EventHandler::KeyUp(SDL_Event event)
{
  m_released[event.key.keysym.sym]= true;
}

void EventHandler::Touch(SDL_Event event)
{
	touch.x = event.tfinger.x;
	touch.y = event.tfinger.y;
    touch.w = TILESIZE;
    touch.h = TILESIZE;
}


bool EventHandler::Pressed(SDL_Keycode code)
{
   return m_pressed[code];
}
bool EventHandler::released(SDL_Keycode code)
{
    return m_released[code];
}
bool EventHandler::held(SDL_Keycode code)
{
    return m_held[code];
}
SDL_Rect EventHandler::getTouch()
{
   return touch;
}

void EventHandler::Clear()
  {
	 m_held.clear();
	 m_pressed.clear();
	 m_released.clear();
  }







int main(int argc, char *argv[])
{

	SDL_Init(SDL_INIT_EVERYTHING);
    SDL_CreateWindowAndRenderer(W,H, SDL_WINDOW_BORDERLESS| SDL_WINDOW_SHOWN, &window, &render);
	bool run = true;
	SDL_Event event;

	if(!LoadResource())
		cout<<"Error Cargando bmps";



	while(run)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			    case SDL_KEYDOWN:
			    	EventHandler::Input()->KeyDown(event);
			     break;
			    case SDL_KEYUP:
			    	EventHandler::Input()->KeyUp(event);
			     break;
			    case SDL_FINGERDOWN:
			    	EventHandler::Input()->Touch(event);
			     break;
			    case SDL_FINGERUP:
			    	EventHandler::Input()->Clear();
			     break;
			}
		}



		if(EventHandler::Input()->Pressed(SDLK_q))
			  run = false;
		else if(EventHandler::Input()->Pressed(SDLK_RIGHT))
			          dir = DERECHA;
		else if(EventHandler::Input()->Pressed(SDLK_LEFT))
					  dir = ISQUIERDA;
		else if(EventHandler::Input()->Pressed(SDLK_UP))
					  dir = ARRIBA;
		else if(EventHandler::Input()->Pressed(SDLK_DOWN))
					  dir = ABAJO;
		else
			          dir = NINGUNO;




       switch(dir)
       {
         case ARRIBA:

        	 if(mapa1[(Pacman_diry-32)/32][Pacman_dirx/32] != 'X')
                    Pacman_diry -=32;

        	 break;
         case ABAJO:
        	 if(mapa1[(Pacman_diry+32)/32][Pacman_dirx/32] != 'X')
        	 Pacman_diry +=32;

           break;
         case DERECHA:
        	 if(mapa1[Pacman_diry/32][((Pacman_dirx+32)/32)] != 'X')
        	 Pacman_dirx +=32;


        	 break;
         case ISQUIERDA:


        	 if(mapa1[Pacman_diry/32][((Pacman_dirx-32)/32)] != 'X')
        	                     Pacman_dirx -=32;


             break;

       }
      if(Pacman_dirx == atajo_isquierdaX  - 32  && Pacman_diry == atajo_isquierdaY  )
         {
    	        Pacman_dirx = atajo_derechaX;
                Pacman_diry = atajo_derechaY;

               // SDL_Log(" atajo derecha %d , %d",atajo_derechaX,atajo_derechaY);

         }
      if(Pacman_dirx == atajo_derechaX +32 && Pacman_diry == atajo_derechaY  )
         {
         	    Pacman_dirx = atajo_isquierdaX;
                Pacman_diry = atajo_isquierdaY;

             // SDL_Log(" atajo derecha %d , %d",atajo_derechaX,atajo_derechaY);

         }
















		SDL_RenderClear(render);
		SDL_SetRenderDrawColor(render, 0xa,0xa,0xa,0xff);
		DrawMap(render);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,dir);

		SDL_RenderPresent(render);
		SDL_Delay(60);
		DrawPersonaje(render, personaje, Pacman_dirx,Pacman_diry,NINGUNO);

		SDL_RenderPresent(render);
		EventHandler::Input()->Clear();

	  if( GameOver(  0  ))
		SDL_Log("Score : [%d]", score);



	}


 SDL_DestroyWindow(window);
 SDL_DestroyRenderer(render);
 return 0;
}













void DrawMap(SDL_Renderer *render)
{

    for(int row = 0 ;row <  MAXFILAS; row++)
    {
    	for(int col= 0; col < MAXCOLS; col++)
    	{

    		if(mapa1[row][col] == 'I')
    		{
    			 atajo_isquierdaX   = col *32 ;
    			 atajo_isquierdaY   = row *32 ;
    			 mapa1[row][col] = 'i';
    		}
    		if(mapa1[row][col] == 'D')
    		  {
    		     atajo_derechaX   = col *32 ;
    		     atajo_derechaY   = row *32 ;
    		     mapa1[row][col] = 'd';
    		 }



#ifndef ANDROID_DEV
    		if(mapa1[row][col] == 'X')
    		{
    			//SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    			DibujarObjecto(render,block1, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		}
    		if(mapa1[row][col] == 'o')
    		  {
    		    //SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    DibujarObjecto(render,moneda, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		     if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		     {
    		    	 score += 10;
    		    	 SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    	 mapa1[row][col] = '0';
    		     }


    		  }
    		if(mapa1[row][col] == 'a')
    		   {
    		    		  //  SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    		    DibujarObjecto(render,rapple, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		    	 if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		    		{
    		    		     SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    		      mapa1[row][col] = '8';
    		    	       score +=100;
    		    		}
        	   }
    		if(mapa1[row][col] == 'A')
    		    {
    		    		    		  //  SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    	DibujarObjecto(render,gapple, col *32,row *32   ,30,30, 0, 0, 32, 32);
    		    	 if(Pacman_dirx/32 == col && Pacman_diry/32 ==row)
    		    		     {
    		    		     SDL_Log("mapa  : [ %d ]  pacman dirx [%d]",mapa1[row][col],Pacman_dirx);
    		    		    mapa1[row][col] = '9';
    		    	         score+=1000;
    		    		     }
    		     }



#else  ///desktop
    		if(mapa1[row][col] == 'X')
    		  {
    		    			SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    			DibujarObjecto(render,block1, row *32,col *32   ,30,30, 0, 0, 32, 32);
    		   }
    		if(mapa1[row][col] == 'o')
    		    {
    		    	 SDL_Log("coordenadas x : [%d] X  Y:  [%d] ", row*32, col*32);
    		    	DibujarObjecto(render,moneda,row *32 , col *32  ,30,30, 0, 0, 32, 32);
    		    }
#endif

    	}
    }

}








SDL_Texture *loadTexture(SDL_Renderer *render, const char* src, SDL_Color color_magic)
{
  SDL_Texture *ret = NULL;
  SDL_Surface *lsf = SDL_LoadBMP(src);
  SDL_Log("Imagen a cargar [%s]", src);

  int t = SDL_SetColorKey(lsf, SDL_TRUE, SDL_MapRGB(lsf->format, color_magic.r, color_magic.g, color_magic.b));
  SDL_Log("SDL_SetColorKey devolvio [%d]", t );

  ret =  SDL_CreateTextureFromSurface(render, lsf);
  SDL_Log("SDL_CreateTextureFromSurface devolvio [%d]", ret);


  return ret;
}




void CanMove(int &x,int &y,int current)
{


}





bool LoadResource()
{
   bool fRet = false;
   block1    = loadTexture(render, "roca1.bmp",transaparent) ;
   block2    = loadTexture(render, "roca2.bmp",transaparent) ;
   enemigos  = loadTexture(render, "enemigo.bmp",transaparent) ;
   personaje = loadTexture(render, "pacman.bmp",transaparent) ;
   moneda    = loadTexture(render, "moneda.bmp",transaparent) ;
   muerte    = loadTexture(render, "muerte.bmp", transaparent);
   arrows1   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows2   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows3   = loadTexture(render, "arrow1.bmp", transaparent);
   arrows4   = loadTexture(render, "arrow1.bmp", transaparent);
   rapple    = loadTexture(render, "rapple.bmp", transaparent);
   gapple    = loadTexture(render, "gapple.bmp", transaparent);

   if(block1   != NULL &&
     block2    != NULL &&
	 enemigos  != NULL &&
	 personaje != NULL &&
	 moneda    != NULL &&
	 arrows1   != NULL &&
     arrows2   != NULL &&
	 arrows3   != NULL &&
	 arrows4   != NULL &&
	 rapple    != NULL &&
	 gapple    != NULL)
	   fRet= true;


   return fRet;

}




void DibujarObjecto(SDL_Renderer *render, SDL_Texture *t1, int x, int y,int w,int h, int row, int col, int rz, int cz)
{
   SDL_Rect offset  = {x,y,w,h};
   SDL_Rect clipset = {row, col, rz,cz};
   SDL_RenderCopy(render, t1, &clipset, &offset);
}





void DrawPersonaje(SDL_Renderer *render, SDL_Texture *t, int x , int y , int FACE)
{
     SDL_Rect offset = {x,y,TILESIZE,TILESIZE};
     SDL_Rect clipset= {33*FACE, 0, TILESIZE,TILESIZE};
     SDL_RenderCopy(render, t, &clipset, &offset);


}






bool checkCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;
    //If any of the sides from A are outside of B
        if( bottomA <= topB )
        {
            return false;
        }

        if( topA >= bottomB )
        {
            return false;
        }

        if( rightA <= leftB )
        {
            return false;
        }

        if( leftA >= rightB )
        {
            return false;
        }

        //If none of the sides from A are outside B
        return true;
    }

bool GameOver(int current )
{
   bool fRet = false;

   for(int row = 0 ;row <  MAXFILAS; row++)
      {
      	for(int col= 0; col < MAXCOLS; col++)
      	{
           if(mapa1[row][col] == 'o' || mapa1[row][col] == 'a' || mapa1[row][col] == 'A' )
        	{
        	        return true;
        	}
      	}
      }



   return fRet ;
}

#endif
