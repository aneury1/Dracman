/*
 * MusicManager.hpp
 *
 *  Created on: 31 de dic. de 2015
 *      Author: aneury
 */

#ifndef MUSICMANAGER_HPP_
#define MUSICMANAGER_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

#include"Device.hpp"

class MusicManager
{
	MusicManager(){}
	static MusicManager* m_instance;
    map<string , Mix_Music *>m_music;
    map<string , Mix_Chunk *>m_sound;

    public:
    static MusicManager* Jukebox()
    {
    	if(m_instance == NULL)
    	  m_instance = new MusicManager();
    	return m_instance;
    }
   void LoadMusic(Device *device, const char *src, string id);
   void LoadWav(Device *device, const char *src, string id);
   void PlayMusic(Device *device, string id , int vol);
   void PlayWav(Device *device, string id);
   void Delete(Device *device, string id);
   void SetVolume(unsigned int l);


};







#endif /* MUSICMANAGER_HPP_ */
