/*
 * StateMachine.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

#include"GameState.hpp"
#include"StateMachine.hpp"
#include"Device.hpp"



StateMachine* StateMachine::m_instance  = 0 ;

void StateMachine::LoadState(unsigned int state, GameState *sonn)
{

    this->m_state[state] = sonn;
}
GameState* StateMachine::getState(unsigned int state)
{
	//SDL_Log("EStado especifico [%d]", state);
    return m_state[state];
}

GameState* StateMachine::getState(Device *device)
{
	///SDL_Log("EStado especifico [%d]", device->current_state);
    return m_state[device->current_state];
}




void StateMachine::ResetMachine()
{
    m_state.clear();
}
