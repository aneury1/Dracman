/*
 * StateMachine.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#ifndef STATEMACHINE_HPP_
#define STATEMACHINE_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

#include"GameState.hpp"


class StateMachine
{

	StateMachine(){}
	static StateMachine* m_instance;
	map<unsigned int, GameState *>m_state;
#if 0

	int last_state;
	int next_state;

#endif

public:
     static StateMachine *stateManager()
     {
    	 if(m_instance==NULL)
    		 m_instance = new StateMachine();
    	 return m_instance;
     }
    void LoadState(unsigned int state, GameState *sonn);
    GameState *getState(unsigned int state);
    GameState *getState(Device *device);
    void ResetMachine();

};




#endif /* STATEMACHINE_HPP_ */
