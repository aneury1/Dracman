/*
 * Timer.hpp
 *
 *  Created on: 2 de ene. de 2016
 *      Author: aneury
 */

#ifndef TIMER_HPP_
#define TIMER_HPP_



#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

#define FRECUENCIA 10

class Timer
{

private:
	Timer()
    {
	}
	static Timer *m_instance;

	unsigned int timer1;
	unsigned int timer2;
	unsigned int timer3;
	unsigned int timer4;

	int fps;
	unsigned int startime;
    unsigned int currentTime;
    //The clock time when the timer started
            Uint32 mStartTicks;

            //The ticks stored when the timer was paused
            Uint32 mPausedTicks;

            //The timer status
            bool mPaused;
            bool mStarted;


public:
    static Timer* timestamp()
    {
    	if(m_instance == NULL)
    	{
    		m_instance = new Timer();
    	}
    	return m_instance;
    }
    unsigned int  StartTime();
    unsigned int  CurrentTime();
    unsigned int  FPS();


           void Reset();
           void start();
           void stop();
           void pause();
           void unpause();

           //Gets the timer's time
           Uint32 getTicks();

           //Checks the status of the timer
           bool isStarted();
           bool isPaused();
           int fps_sincronizar (void);




























};




#endif /* TIMER_HPP_ */
