/*
 * GameEngine.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

///project include here
#include"todo.hpp"
#include"InputHandler.hpp"
#include"GraphicsManager.hpp"
#include"StateMachine.hpp"
#include"GameState.hpp"
#include"IntroState.hpp"
#include"OptionState.hpp"
#include"mapa.hpp"
#include"Object.hpp"
#include"Timer.hpp"
#include"MusicManager.hpp"
#include"Device.hpp"
#include"PlayState.hpp"

#ifdef OPENGL_SDL_ENABLE
void Screenshot(int x, int y, int w, int h, const char * filename)
{
    unsigned char * pixels = new unsigned char[w*h*4]; // 4 bytes for RGBA
    glReadPixels(x,y,w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

    SDL_Surface * surf = SDL_CreateRGBSurfaceFrom(pixels, w, h, 8*4, w*4, 0,0,0,0);
    SDL_SaveBMP(surf, filename);

    SDL_FreeSurface(surf);
    delete [] pixels;
}
#endif



bool saveScreenshotBMP(std::string filepath, SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
    SDL_Surface* saveSurface = NULL;
    SDL_Surface* infoSurface = NULL;
    infoSurface = SDL_GetWindowSurface(SDLWindow);
    if (infoSurface == NULL) {
        std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
    } else {
        unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
        if (pixels == 0) {
            std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
            return false;
        } else {
            if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
                std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
                pixels = NULL;
                return false;
            } else {
                saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
                if (saveSurface == NULL) {
                    std::cerr << "Couldn't create SDL_Surface from renderer pixel data. SDL_GetError() - " << SDL_GetError() << "\n";
                    return false;
                }
                SDL_SaveBMP(saveSurface, filepath.c_str());
                SDL_FreeSurface(saveSurface);
                saveSurface = NULL;
            }
            delete[] pixels;
        }
        SDL_FreeSurface(infoSurface);
        infoSurface = NULL;
    }
    return true;
}









GameEngine::GameEngine()
{
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    Mix_Init(MIX_INIT_FLAC     | MIX_INIT_MOD |  MIX_INIT_MODPLUG  |     MIX_INIT_MP3 | MIX_INIT_OGG | MIX_INIT_FLUIDSYNTH );
    IMG_Init(IMG_INIT_JPG       | IMG_INIT_PNG |  IMG_INIT_TIF      |     IMG_INIT_WEBP );
    Mix_Init( MIX_INIT_FLAC     |MIX_INIT_MOD       |   MIX_INIT_MODPLUG    | MIX_INIT_MP3        | MIX_INIT_OGG        | MIX_INIT_FLUIDSYNTH );
    device = new Device();
    //Initialize SDL_mixer
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
      {
                       device->Running = false;
      }




#ifndef ANDROID_DEV

    SDL_CreateWindowAndRenderer(W,H,SDL_WINDOW_BORDERLESS  |SDL_WINDOW_FULLSCREEN, &device->window,&device->render);
    SDL_SetWindowTitle(device->window, TITLE);
    SDL_SetWindowPosition(device->window, SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED);
    device->window_param. w = W;
    device->window_param. h = H;
    device->window_param. x = device->window_param.y = SDL_WINDOWPOS_CENTERED;
    ///SDL_StartTextInput();


#else
///todo
    SDL_Rect ScreenRect;
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
    		{
    			SDL_Log( "Warning: Linear texture filtering not enabled!" );
    		}

            //Get device display mode
            SDL_DisplayMode displayMode;
            if( SDL_GetCurrentDisplayMode( 0, &displayMode ) == 0 )
            {
                ScreenRect.w = displayMode.w;
                ScreenRect.h = displayMode.h;
            }
            SDL_CreateWindowAndRenderer(ScreenRect.w,ScreenRect.h,SDL_WINDOW_BORDERLESS, &device->window,&device->render);
            SDL_SetWindowTitle(device->window, TITLE);
            SDL_SetWindowPosition(device->window, SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED);
            device->window_param. w = ScreenRect.w;
            device->window_param. h = ScreenRect.h;
            device->window_param. x = device->window_param.y = SDL_WINDOWPOS_CENTERED;
#endif
   device->Running = true;
   SDL_ShowCursor(false);

  device->current_state  = PLAYSTATE;
   //device->current_state  = MENUSTATE;

   SDL_Color color={0xaa,0xff,0xaa,0xff};
   SDL_Color color1={0xaa,0xac,0xff,0xff};
   GraphicsManager::Graphics()->LoadImg(device,"title.bmp","title" );
   GraphicsManager::Graphics()->LoadImg(device,"gusano_title.bmp","puntero" );
   GraphicsManager::Graphics()->LoadImg(device, "bg1.bmp", "bg1");
   GraphicsManager::Graphics()->LoadImg(device,"tileset1.bmp","tileset" );

   GraphicsManager::Graphics()->LoadFont(device,"lazy.ttf",18);
   GraphicsManager::Graphics()->CreateText(device,"INIT","Inicio",color);
   GraphicsManager::Graphics()->CreateText(device,"OPT","Opciones",color1);

   StateMachine::stateManager()->LoadState(PLAYSTATE, new PlayState());
   StateMachine::stateManager()->LoadState(INTROSTATE, new IntroState());
   StateMachine::stateManager()->LoadState(MENUSTATE, new OptionState());
   MusicManager::Jukebox()->LoadWav(device,"medium.wav","beep");
   StateMachine::stateManager()->getState(device->current_state)->init(device);


   device->test = 32;






}

void GameEngine::Input()
{
    while(SDL_PollEvent(&device->event))
    {
      switch(device->event.type)
      {
         case SDL_QUIT:
        	  run = false;
        	  break;
         case SDL_KEYDOWN:
             InputHandler::InputManager()->KeyDown(device->event);
             InputHandler::InputManager()->KeyboardInput(event);
        	 break;
         case SDL_KEYUP:
        	 InputHandler::InputManager()->KeyUp(device->event);
        	  break;
         case  SDL_MOUSEMOTION:
        	 InputHandler::InputManager()-> MouseMotionEvent(event);
              break;
         case SDL_MOUSEBUTTONDOWN:
        	 InputHandler::InputManager()->MouseDownEvent(event);
        	 break;
         case SDL_MOUSEBUTTONUP:
        	 InputHandler::InputManager()->MouseUpEvent(event);
        	 break;
         case SDL_TEXTINPUT:
        	 InputHandler::InputManager()->KeyboardInput(event);
        	 break;
#ifdef ANDROID_DEV
         case  SDL_FINGERDOWN:
        	  break;
         case SDL_FINGERUP:
              break;
         case SDL_FINGERMOTION:
        	  break;

#endif


      }
    }
}


void GameEngine::Update()
{



if(InputHandler::InputManager()->IsPressed(SDLK_q))
	device->Running = false;

StateMachine::stateManager()->getState(device->current_state)->Update(device);




}

void GameEngine::Render()
{

   SDL_RenderClear(device->render);
   //SDL_SetRenderDrawColor(device->render, 10,10,10,0xFF);
   GraphicsManager::Graphics()->DrawImg(device, "bg1",0,0,640,480);
    StateMachine::stateManager()->getState(device->current_state)->Draw(device);
   SDL_RenderPresent(device->render);
   Timer::timestamp()->fps_sincronizar();
   InputHandler::InputManager()->clear();
}

void GameEngine::Quit()
{

   SDL_DestroyRenderer(device->render);
   SDL_DestroyWindow(device->window);
   SDL_Quit();
}

bool GameEngine::Run()
{
   return device->Running;
}
void GameEngine::setRun(bool R)
{
    device->Running = R;
}
