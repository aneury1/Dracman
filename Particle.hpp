/*
 * Particle.hpp
 *
 *  Created on: 5 de ene. de 2016
 *      Author: aneury
 */

#ifndef PARTICLE_HPP_
#define PARTICLE_HPP_




#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;



class Particle
{

protected:
	int x;
	int y;
	string id;
	bool isActive;
	int frame;
public:

	Particle(int x, int y);
	void Draw(Device *devie);
	bool IsActive();



};



#endif /* PARTICLE_HPP_ */
