/*
 * Object.hpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */

#ifndef OBJECT_HPP_
#define OBJECT_HPP_



#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;

#include"Device.hpp"

#define OBJECT_UP             0
#define OBJECT_DOWN           1
#define OBJECT_LEFT           2
#define OBJECT_RIGHT          3
#define OBJECT_ANIMATE        4
#define OBJECT_ANIMATE_STATIC 5
#define RUN 12
#define UP     1
#define DOWN   2
#define LEFT   3
#define RIGTH  4






class 	Object
{


private:
	SDL_Rect offset;
	SDL_Rect clipset;
	unsigned int current_face;
	string id;
	int xvel;
	int yvel;
	bool isVisible;
	int hp;
	int power;
	float gravity;
	 int lim_w;
	int lim_h;

public:
	Object(Device *device,int x, int y, int OBJ_SIZE_W,int OBJ_SIZE_H, int curren_face,string ids);

	~Object();
	void SetXvelocity(int xv);
	void SetYvelocity(int yv);
	int  getXvel();
	int getYvel();


	void SetXPosition(int x);
	void setYPosition(int y);
	void SetOffset(SDL_Rect r);
	void SetWidth(int w);
	void SetHeight(int h);
	int getXposition();
	int getYposition();
	int getWidth();
	int getHeight();
	SDL_Rect GetOffset();


	void SetClipset(SDL_Rect c);
    SDL_Rect GetClipset();
    void SetRow(int r);
    void SetCol(int c);
    void SetRowSize(int rs);
    void SetColSize(int cs);
    int getRow();
    int getCol();
    int getRowSize();
    int getColSize();






	void SetVisible(bool v);
	bool IsVisible();

    void SetHp(int hpd);
    int getHP();

    void SetPower(int powerh);
    int getPower();

    void SetGravity(float g);
    float getGetGravity();

    void SetId(string id);
    string getId();


  ////Center the camera over the dot
  //    camera.x = ( dot.getPosX() + Dot::DOT_WIDTH / 2 ) - SCREEN_WIDTH / 2;
  //  camera.y = ( dot.getPosY() + Dot::DOT_HEIGHT / 2 ) - SCREEN_HEIGHT / 2;


   void  Move(int where, int Npad);
   //void ObjectDraw(Device *device);

};




#endif /* OBJECT_HPP_ */
