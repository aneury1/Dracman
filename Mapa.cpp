/*
 * Mapa.cpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */







#include<cstring>
#include<iostream>
using namespace std;
#include"staticresource.hpp"
#include"Device.hpp"
#include"mapa.hpp"
#include"GraphicsManager.hpp"


Map*Map::m_instance = 0;




void Map::DrawMap1(Device *device)
{


    for(int r = 0; r < ROW - 1 ; r++)
    {
        string id;
    	cout <<"R "<<r <<"  ";
    	for(int c= 0; c < COL - 1; c++)
    	{
    		cout << mapa1[r][c]<<",";
            switch(mapa1[r][c])
            {
              case '0':
                  id = "nada";

                  break;
              case '1':
                  id = "bloque";
                  GraphicsManager::Graphics()->DrawImg(device, "tileset",  c *29,r *32   ,30,30, 0, 0, 32, 32);
                  break;
              case '2':
            	  GraphicsManager::Graphics()->DrawImg(device, "tileset",  c *29,r *32   ,32,32, 96, 0, 32, 32);
            	  id = "moneda";
            	  break;
              case '3':
            	  id = "token";
            	  break;
              case '4':
            	  id= "player";
            	  break;
              case '@':
            	  id = "especial";
            	  break;
              default:
            	  cout <<"Ignorado";
            	  break;
            }
    	}
    	cout <<endl ;
    }


}


void Map::loadTileMap(Device *device,const char *src, string id,bool add )
{
   ifstream fs;
   int x      = 0;
   int y      = 0;
   char c     = '\0';
   int width  = 0;
   int height = 0;
   string map_tileset_name;
   string cmd;
   vector<Tile *>m_result;

   ofstream log("log.txt");


	fs.open(src);
	if(!fs.is_open())
	{
		SDL_Log("Error Abriendo el archivo");
	}
	else
	{
		SDL_Log("archivo abierto parseando");
       fs >> cmd;
       if((strcmp(cmd.c_str(), "begin") == 0 ))
		{

    	   SDL_Log("Begin");
           fs >> width;
           fs >> height;
           fs >> map_tileset_name;
           SDL_Log("%d, %d",width, height);
           log << width <<"  "<<height <<endl;
		}
       else
       {
    	 SDL_Log("el mapa no parece valido");
    	 device->Running = false;
       }
       fs >> cmd;
       if((strcmp(cmd.c_str(), "end") == 0 ))
       	{

    	 SDL_Log("bien ahora a cargar tiles");
       	}
       else
       	{
    	   SDL_Log("error");
    	   device->Running = false;
    	   cin.get();
       	}

       while(fs.good() || !fs.fail())
       {
    	   c = fs.get();

    	   if(c == LAST_CHARATER_IN_MAP)
    	   {
    		   cout << '\n' << ',';
    	   }
    	   else if(c == BREAK_MAP)
    	   {
    		   cout <<"Fin de la ejecucion\n";
    		   break;
    	   }
    	   else
    	   {
    		   cout <<',' << c;
    	   }
    	  if(c != BREAK_MAP || c != LAST_CHARATER_IN_MAP|| c != ' ' || c != '\t' )
    	  {
				if(c != '@')
				{


				   Tile *tile = new Tile(x, y, c);
				   x += TILE_W;
				   if(x >= width)
				   {
					   x  = 0;
					   y += TILE_H;
				   }
				   SDL_Log("x[%d],y[%d], type[%c]",x,y,c);
				   log <<"X "<< x << "   y  " <<y <<"   type  "<<c <<endl;
				   m_result.push_back(tile);
				   SDL_Log("Vector.size() = [%d]",m_result.size());


                   }


       }

   log.flush();
   log.close();
   _map[id] = m_result;

   cout << "El campo " << id <<" tiene" << _map[id].size()<<"  elementos\n";




	}
}
}


/**
 * @function DrawMap
 * @brief esta funcion se utiliza para dibujar el mapa en pantalla, aunque es primitiva y se debe mejorar debe trabajar para el engine
 *        1 = tile tierra en n = 1 y = 0
 *        2 = tile tierra en n = 2 y = 0
 *        3 = tile tierra en n = 3 y = 0
 *
 *
 */
void Map::DrawMap(Device *device, string id)
{




	 for(unsigned int iter = 0 ; iter < _map[id].size(); iter++)
	 {
         int row = 0;
         int col = 0;

	     switch(_map[id].at(iter)->getType())
	     {
	       case '0':

	    	   row = 3;
	    	   col = 5;
                                                                          /** en caso de querer dibujar despegado del border  + 32*/
	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,row,col,32,32 );

	    	   break;
	       case '1':
	    	   row = 48 * 1 + 3;
	    	   col =  5;

	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,3,5,32,32 );
	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX() - 16, _map[id].at(iter)->getY()+16,device->test,device->test,row,col,32,32 );

	    	   break;
	       case '2':
	    	   row = 101;
	    	   col = 5;
	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,3,5,48,48 );
	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX() - 16 , _map[id].at(iter)->getY()+16,device->test,device->test,row,col,48,48 );

 	           break;
	       case '3':

	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153,5,45,48 );
	   	   break;
	       case '4':

	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153 + 48,5,45,48 );

	    	   break;
	       case '5':
	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153+48+48,5,45,48 );

               break;
	       case '6':
		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153,5,45,48 );
		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,395,153,45,48 );
	             break;
	       case '7':
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153,5,45,48 );
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,395+48,153,45,48 );
	       	             break;
	       case '8':
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153+48+48,5,45,48 );
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,395,153+48,45,48 );
	       	             break;
	       case '9':
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,153+48+48,5,45,48 );
	       		    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,395+48,153+48,45,48 );
	       	             break;
	       case 'd':

	    	   GraphicsManager::Graphics()->DrawImg(device,"map", _map[id].at(iter)->getX()- 16, _map[id].at(iter)->getY() +16,device->test,device->test,53,249,45,48 );




	       default:

	    	   break;
	     }

	 }

cout <<device->test <<endl;

}



