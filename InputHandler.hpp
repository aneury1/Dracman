/*
 * InputHandler.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#ifndef INPUTHANDLER_HPP_
#define INPUTHANDLER_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;


#define  TOUCH_SIZE   32
#define  MOUSE_CURSOR 42

class InputHandler
{
	InputHandler()
	{

	}
	static InputHandler* m_instance;
	map< SDL_Keycode, bool  >m_held;
	map< SDL_Keycode, bool  >m_released;
	map< SDL_Keycode, bool  >m_pressed;
	///const Uint8* currentKeyStates =   SDL_GetKeyboardState( NULL ); if map is not avaliable you always can use it.
	bool AnyKey;
    SDL_Rect touch;
    SDL_Rect Mouse;
    bool Down;
    string str1;
    stringstream str2;


public:
    static InputHandler* InputManager()
    {
    	if(m_instance == NULL)
    		m_instance = new InputHandler();
    	return m_instance;
    }

	void KeyDown(SDL_Event event);
	void KeyUp(SDL_Event event);
    bool IsPressed(SDL_Keycode code);
	bool IsHeld(SDL_Keycode code);
	bool IsReleased(SDL_Keycode code);
	void TouchEvent(SDL_Event event);
	void MouseMotionEvent(SDL_Event event);
    void MouseDownEvent(SDL_Event event);
    void MouseUpEvent(SDL_Event event);


    void KeyboardInput(SDL_Event event);
    void WindowEvent(SDL_Event event);

	SDL_Rect getTouch();
	SDL_Rect getMouse();
    bool     isMouseDown();
	void clear();
	bool isAnyKey()
	{
		return AnyKey;
	}

	string getText();


#ifdef ANDROID_DEV
    bool TouchEvent(SDL_Rect sc);
#endif




};



#endif /* INPUTHANDLER_HPP_ */
