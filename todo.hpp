

/*
 * todo.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury perez
 *      este pequeno juego es para ser probado en varios dispositivos
 *      a la vez
 *
 *
 *
 */

#ifndef TODO_HPP_
#define TODO_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>

///PARA PROBAR DEBO BORAR

#include<vector>

using namespace std;

#include"Device.hpp"
#include"Object.hpp"
#include"mapa.hpp"
static const int W = 640;
static const int H = 480;
static const char *TITLE = "Dracman";


#define INTROSTATE 1
#define MENUSTATE  2
#define PLAYSTATE  3





class GameEngine
{
    Device       *device;
    SDL_Event     event;
    bool          run;
    Map *b;



    vector<SDL_Rect> m_paint;

public:
    GameEngine();
    void Input();
    void Update();
    void Render();
    void Quit();
    bool Run();
    void setRun(bool R);
};




#endif /* TODO_HPP_ */
