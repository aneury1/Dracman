/*
 * PlayState.hpp
 *
 *  Created on: 14 de ene. de 2016
 *      Author: aneury
 */

#ifndef PLAYSTATE_HPP_
#define PLAYSTATE_HPP_

#include"GameState.hpp"
#include"Object.hpp"


class PlayState : public GameState
{

Object *player;


public:
	   virtual ~PlayState(){};
	   virtual void init(Device *device) ;
	   virtual void Update(Device *device) ;
	   virtual void Draw(Device *device);



};



#endif /* PLAYSTATE_HPP_ */
