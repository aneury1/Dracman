/*
 * InputHandler.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#include"InputHandler.hpp"


InputHandler* InputHandler::m_instance = 0;



void InputHandler::KeyDown(SDL_Event event)
{
   m_held[event.key.keysym.sym]= true;
   m_pressed[event.key.keysym.sym]= true;
   AnyKey = true;
}
void InputHandler::KeyUp(SDL_Event event)
{
	m_released[event.key.keysym.sym]= true;
    AnyKey = false;
}
bool InputHandler::IsPressed(SDL_Keycode code)
{
   return m_pressed[code];
}
bool InputHandler::IsHeld(SDL_Keycode code)
{
   return m_held[code];
}
bool InputHandler::IsReleased(SDL_Keycode code)
{
   return m_released[code];
}
void InputHandler::clear()
{
    m_released.clear();
    m_held.clear();
    m_pressed.clear();
    touch.x = -10;
    touch.y = -10;
    touch.w = touch.h = TOUCH_SIZE;
    Mouse.x = Mouse.y  =-10;
    Mouse.h = Mouse.w  = MOUSE_CURSOR;

}


void InputHandler::MouseMotionEvent(SDL_Event event)
{
	 SDL_GetMouseState( &Mouse.x, &Mouse.y );
     Mouse.w = Mouse.h = MOUSE_CURSOR;
}

void InputHandler::MouseDownEvent(SDL_Event event)
{
	 SDL_GetMouseState( &Mouse.x, &Mouse.y );
	 Mouse.w = Mouse.h = MOUSE_CURSOR;
	 Down = true;
}



void InputHandler::MouseUpEvent(SDL_Event event)
{
	 SDL_GetMouseState( &Mouse.x, &Mouse.y );
     Mouse.w = Mouse.h = MOUSE_CURSOR;
	 Down = false;
}


void InputHandler::TouchEvent(SDL_Event event)
{
   touch.x = event.tfinger.x;
   touch.y = event.tfinger.y;
   touch.w = touch.h = TOUCH_SIZE;
}
SDL_Rect InputHandler::getTouch()
{
   return  touch;
}
SDL_Rect InputHandler::getMouse()
{
   return Mouse;
}
bool     InputHandler::isMouseDown()
{
   return Down;
}




void InputHandler::KeyboardInput(SDL_Event event)
{}
void InputHandler::WindowEvent(SDL_Event event){}






