/*
 * main.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */


#include"todo.hpp"



#include"MusicManager.hpp"
#include"Device.hpp"

#include"mapa.hpp"

#include<cstring>
#include<vector>
#include<iostream>
using namespace std;



int main(int argc, char *argv[])
{





	GameEngine *ge = new GameEngine();


	while(ge->Run())
	{

		ge->Input();
		ge->Update();
		ge->Render();
	}

	ge->Quit();


return 0;
}




/*
   reconstruccion de scroll.c
   de loserjuegos

    migracion de SDL1.2 A SDL2




#ifdef ANDROID_DEV

#include <SDL.h>
#include <SDL_image.h>

#else

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COLUMNAS_TILES 7
#define FILAS_MAPA 10
#define COLUMNAS_MAPA 20
#define ALTO_TILE 48
#define ANCHO_TILE 48
#define FRECUENCIA 10
#define TANQUE_VELOCIDAD 1

typedef struct tanque
{
	int x;
	int y;
	int cuadro;
	SDL_Texture* ima;
}Tanque;

typedef struct camara
{
	float x;
	float y;
} Camara;

SDL_Window   *window;
SDL_Renderer *renderer;
bool          run;



SDL_Texture *tanque_i;
SDL_Texture *tiles_i;


///prototipos de funcion

void Draw(SDL_Renderer *render, SDL_Texture *texture, )



void mover_camara (Camara * camara, Tanque * tanque);
void mover_tanque (Tanque * tanque);
void imprimir_escenario ( SDL_Texture * ima, int x, int y);
void imprimir_tanque ( Tanque * tanque, Camara * camara);
void  iniciar_sdl (const char * titulo);
void imprimir_grilla ( SDL_Texture* src, int cuadro, int cols, int x, int y);
void cargar_imagenes ();
int fps_sincronizar (void);
void iniciar_camara (Camara * camara);
void iniciar_tanque (Tanque * tanque);

int main (int argc, char * argv [])
{
	SDL_Event event;
	int salir = 0;
	int repeticiones;
	int i;
	Tanque tanque;
	Camara camara;

	 iniciar_sdl ("Desplazamiento suave");

	if (window == NULL)
		return 1;

	iniciar_camara (& camara);
	iniciar_tanque (& tanque);

	cargar_imagenes ();


	while ( run)
	{
		while (SDL_PollEvent (& event))
		{
			if (event.type == SDL_QUIT)
				salir = 1;

			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					salir = 1;
			}
		}

		repeticiones = fps_sincronizar ();

		for (i = 0; i < repeticiones; i ++)
		{
			//mover_tanque (& tanque);
			//mover_camara (& camara, & tanque);
		}

		imprimir_escenario (tiles_i, camara.x, camara.y);
		imprimir_tanque ( &tanque, & camara);

		SDL_RenderPresent(renderer);
	}


	SDL_Quit ();
	return 0;
}













void mover_camara (Camara * camara, Tanque * tanque)
{
	int limite_derecho = (COLUMNAS_MAPA - 1) * ANCHO_TILE - 320;
    SDL_Log("Limite derecho [ %d ] ", limite_derecho);
	int limite_inferior = (FILAS_MAPA - 1) * ANCHO_TILE - 240;
	SDL_Log("Limite inferior [ %d ] ", limite_derecho);
	/ * se acerca a la posici�n del tanque de manera gradual * /

	camara->x += (tanque->x - camara->x) / 20;
	camara->y += (tanque->y - camara->y) / 20;


	/* detiene el desplazamiento en los l�mites * /

	if (camara->x < 0)
		camara->x = 0;

	if (camara->y < 0)
		camara->y = 0;

	if (camara->x > limite_derecho)
		camara->x = limite_derecho;

	if (camara->y > limite_inferior)
		camara->y = limite_inferior;
}




void mover_tanque (Tanque * tanque)
{
	const Uint8 * teclas = SDL_GetKeyboardState( NULL );

	SDL_Log("--Mover_tanque--");

	if (teclas [SDLK_LEFT])
	{
		tanque->x -= TANQUE_VELOCIDAD;
		tanque->cuadro = 6;
	}

	if (teclas [SDLK_RIGHT])
	{
		tanque->x += TANQUE_VELOCIDAD;
		tanque->cuadro = 2;
	}

	if (teclas [SDLK_DOWN])
	{
		tanque->y += TANQUE_VELOCIDAD;
		tanque->cuadro = 4;
	}

	if (teclas [SDLK_UP])
	{
		tanque->y -= TANQUE_VELOCIDAD;
		tanque->cuadro = 0;
	}


	if (teclas [SDLK_UP] && teclas [SDLK_LEFT])
		tanque->cuadro = 7;

	if (teclas [SDLK_UP] && teclas [SDLK_RIGHT])
		tanque->cuadro = 1;

	if (teclas [SDLK_DOWN] && teclas [SDLK_LEFT])
		tanque->cuadro = 5;

	if (teclas [SDLK_DOWN] && teclas [SDLK_RIGHT])
		tanque->cuadro = 3;
}






void imprimir_escenario ( SDL_Texture * ima, int x, int y)
{
	static char mapa [FILAS_MAPA] [COLUMNAS_MAPA + 1] = {
		"00000002115000000211",
		"00000002115000000211",
		"00000002115000000211",
		"00000002115000000211",
		"33333334116333333411",
		"11111111111111111111",
		"11111111111111111111",
		"11111111111111111111",
		"11111111111111111111",
		"11111111111111111111"};
	int i;
	int j;
	int fila = y / ALTO_TILE;
	int columna = x / ANCHO_TILE;
	int desplazamiento_x = x % ANCHO_TILE;
	int desplazamiento_y = y % ALTO_TILE;

	for (i = 0; i < 6; i ++)
	{
		for (j = 0; j < 8; j ++)
		{
			imprimir_grilla ( ima,
					mapa [fila + i] [columna + j] - '0',
					COLUMNAS_TILES,
					j * ANCHO_TILE - desplazamiento_x,
					i * ALTO_TILE - desplazamiento_y);
		}
	}

}




void imprimir_tanque ( Tanque * tanque, Camara * camara)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	int x = tanque->x - camara->x + 160;
	int y = tanque->y - camara->y + 120;

	imprimir_grilla ( tanque->ima, tanque->cuadro, 8, x, y);
}




void  iniciar_sdl (const char * titulo)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	SDL_Init(SDL_INIT_EVERYTHING);

    window   = SDL_CreateWindow("tanque v2 ", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 320, 240, SDL_WINDOW_BORDERLESS| SDL_WINDOW_BORDERLESS);

	renderer = SDL_CreateRenderer(window , -1 , SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	run = true;

	SDL_ShowCursor (SDL_DISABLE);

    IMG_Init( IMG_INIT_PNG);

}



void imprimir_grilla ( SDL_Texture* src, int cuadro, int cols, int x, int y)
{
	SDL_Log(" -- %s --", __FUNCTION__);

	int qw = 0, qh =0 ;
	SDL_QueryTexture(src,NULL,NULL,&qw,&qh);
	SDL_Log("SDL_QueryTexture devolvio :  w = [ %d ], h = [ %d ] ", qw, qh);

	int w = qw / cols;
	SDL_Rect fuente = {cuadro * w, 0, w, qh};
	SDL_Rect rect = {x, y, 0, 0};





	if (cuadro < 0 || cuadro > cols)
	{
		printf ("No se puede imprimir el cuadro %d\n", cuadro);
		return;
	}

	//SDL_BlitSurface (src, & fuente,	screen, &rect);

	SDL_RenderCopy(renderer, src ,&fuente, &rect);


}

void cargar_imagenes ()
{
	SDL_Log(" -- %s --", __FUNCTION__);
	SDL_Surface * tmp1 = NULL;
    SDL_Surface * tmp2 = NULL;
	tmp1 = IMG_Load ("tiles.png");
	tmp2 = IMG_Load ("tanques.png");
    if(tmp1 ==NULL)
    {
    	SDL_Log("tmp1");
    }
    if(tmp1 ==NULL)
     {
        	SDL_Log("tmp2");
     }

	tanque_i = SDL_CreateTextureFromSurface(renderer, tmp1);
	tiles_i  = SDL_CreateTextureFromSurface(renderer, tmp2);
	if(tanque_i ==NULL)
	  {
	    	SDL_Log("tanque");
	 }

	if(tiles_i ==NULL)
		  {
		    	SDL_Log("tiles_i");
		 }






}

int fps_sincronizar (void)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	static int t;
	static int tmp;
	static int tl = 0;

	t = SDL_GetTicks ();

	if (t - tl >= FRECUENCIA)
	{
		tmp = (t - tl) / FRECUENCIA;
		tl += tmp * FRECUENCIA;
		return tmp;
	}
	else
	{
		SDL_Delay (FRECUENCIA - (t - tl));
		tl += FRECUENCIA;
		return 1;
	}
}


void iniciar_camara (Camara * camara)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	camara->x = 0;
	camara->y = 0;
}


void iniciar_tanque (Tanque * tanque)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	tanque->x = 250;
	tanque->y = 150;
	tanque->cuadro = 0;
	tanque->ima= tanque_i;
}


*/






