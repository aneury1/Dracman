/*
 * mapa.hpp
 *
 *  Created on: 30 de dic. de 2015
 *      Author: aneury
 */

#ifndef MAPA_HPP_
#define MAPA_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif

#include<vector>
#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;

#include"Tile.hpp"
#include"Device.hpp"

#define BREAK_MAP            '#'
#define LAST_CHARATER_IN_MAP '@'
#define WATER_TILE           'w'
#define EARTH_TILE           'e'
#define GRASS_TILE           'g'

#define TILE_W             32
#define TILE_H             32

static map<string, vector<Tile *> > _map;

typedef struct TAGTeleport
{
	int x;
	int y;
	string map_id_source;
    string map_id_dest;
}Teleporter;





class Map
{
   Map(){}
   static Map* m_instance;

   int x, y;


public:

   static Map* Mapper()
   {
	   if(m_instance== NULL)
		   m_instance = new Map();
	   return m_instance;
   }

	void DrawMap1(Device *device);
	void DrawMap(Device* device, string id);
    void loadTileMap(Device *device,const char *src, string id,bool add =true);
  /*  void SetPlayerPosition(Object *obj);
    void SetObjects(Object **objs);
    */

};




#endif /* MAPA_HPP_ */
