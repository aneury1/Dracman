/*
 * IntroState.cpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */







#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif

#include<iostream>
#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

///project include here
#include"GameState.hpp"
#include"InputHandler.hpp"
#include"IntroState.hpp"
#include"GraphicsManager.hpp"
#include"MusicManager.hpp"


IntroState::IntroState()
{


}



void IntroState::init(Device *device)
{
	puntero_intro = new Object(device,device->window_param.w /2 - 132,device->window_param.h/2,32,32,UP,"puntero_init");
}



IntroState::~IntroState()
{


}
void IntroState::Update(Device *device)
{
	static bool inicio = true;
	 if(InputHandler::InputManager()->IsPressed(SDLK_UP))
	 {
		 MusicManager::Jukebox()->PlayWav(device,"beep");
	    if(inicio == true)
		   inicio = false;
	    else
	       inicio = true;

		if(inicio == true)
	    	puntero_intro->setYPosition(device->window_param.h/2);
	    else
	    	puntero_intro->setYPosition(device->window_param.h/2+ 60);
	 }
	 if(InputHandler::InputManager()->IsPressed(SDLK_DOWN))
	  {
		 if(inicio == true)
			   inicio = false;
		    else
		       inicio = true;

			if(inicio == true)
		    	puntero_intro->setYPosition(device->window_param.h/2);
		    else
		    	puntero_intro->setYPosition(device->window_param.h/2+ 60);
			MusicManager::Jukebox()->PlayWav(device,"beep");

	  }

}
void IntroState::Draw(Device *device)
{

	   GraphicsManager::Graphics()->DrawImg(device,"title",0,0,device->window_param.w,device->window_param.h);
	   GraphicsManager::Graphics()->DrawImg(device,"INIT",device->window_param.w /2 - 100, device->window_param.h/2,192,50);
	   GraphicsManager::Graphics()->DrawImg(device,"OPT",device->window_param.w /2 - 100, device->window_param.h/2 + 60,192,50);
	   GraphicsManager::Graphics()->DrawImg(device,"puntero",puntero_intro->getXposition(),puntero_intro->getYposition(),32,32,0,0,32,32);

}
