/*
 * OptionState.hpp
 *
 *  Created on: 11 de ene. de 2016
 *      Author: aneury
 */



#pragma once






#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;

///project include here
#include"GameState.hpp"
#include"InputHandler.hpp"
#include"GraphicsManager.hpp"
#include"Object.hpp"
#include"Device.hpp"

#define HI 64
#define WS 68


class OptionState : public GameState
{


	Object *puntero_intro;

public:
	   OptionState();
	   virtual ~OptionState() ;
	   virtual void init(Device *device);
	   virtual void Update(Device *device);
	   virtual void Draw(Device *device);
};



