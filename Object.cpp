/*
 * Object.cpp
 *
 *  Created on: 2 de ene. de 2016
 *      Author: aneury
 */


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;


#include"Object.hpp"



Object::Object(Device *device,int x, int y, int OBJ_SIZE_W,int OBJ_SIZE_H, int curren_face,string ids)
{
	 offset  = {x,y,OBJ_SIZE_W, OBJ_SIZE_H};
	 clipset = {0,0,OBJ_SIZE_W,OBJ_SIZE_H};
	 current_face = curren_face;
     id = ids;
	 xvel  = 0;
     yvel  = 0;
	 isVisible = true;
	 hp = 0;
	 power = 0;
	 gravity = 0;
    lim_w = device->window_param.w;
    lim_h = device->window_param.h;
}
Object::~Object()
{
	 offset  = {0,0,0, 0};
	 clipset = {0,0,0,0};
	 current_face = -1;
	 xvel  = 0;
     yvel  = 0;
	 isVisible = false;
	 hp = 0;
	 power = 0;
	 gravity = 0;
}
void Object::SetXvelocity(int xv)
{
   xvel = xv;
}
void Object::SetYvelocity(int yv)
{
  	yvel = yv;
}


int  Object::getXvel()
{
   return xvel;
}

int Object::getYvel()
{
   return yvel;
}
void Object::SetXPosition(int x)
{
     offset.x = x;
}
 void Object::setYPosition(int y)
 {
	 offset.y = y;
 }

 void Object::SetWidth(int w)
 {
	 offset.w = w;
 }
 void Object::SetHeight(int h)
 {
	 offset.h = h;
 }

 int Object::getXposition()
 {
	return offset.x;
 }
 int Object::getYposition()
 {
	 return offset.y;
 }
 int Object::getWidth()
 {
	 return offset.w;
 }
 int Object::getHeight()
 {
	 return offset.h;
 }



 void Object::SetRow(int r)
 {
	 clipset.x = r;
 }
 void Object::SetCol(int c)
 {
	clipset.y = c;
 }
 void Object::SetRowSize(int rs)
 {
	 clipset.w = rs;
 }
 void Object::SetColSize(int cs)
 {
	 clipset.h = cs;
 }
 int Object::getRow()
 {
	 return clipset.x;
 }
 int Object::getCol()
 {
	 return clipset.y;
 }
 int Object::getRowSize()
 {
	 return clipset.w;
 }
 int Object::getColSize()
 {
	 return clipset.h;
 }

void Object::SetId(string ids)
{
   id = ids;
}
string Object::getId()
{
    return id;
}


















void Object::SetVisible(bool v)
{
    isVisible =v;
}
bool Object::IsVisible()
{
    return isVisible;
}
void Object::SetOffset(SDL_Rect r)
{
    offset.x = r.x;
    offset.y = r.y;
    offset.w = r.w;
    offset.h = r.h;
}
void Object::SetClipset(SDL_Rect c)
{
    clipset.x = c.x;
    clipset.y = c.y;
    clipset.w = c.w;
    clipset.h = c.h;
}
SDL_Rect Object::GetOffset()
{
  return offset;
}
SDL_Rect Object::GetClipset()
{
  return clipset;
}
void Object::SetHp(int hpd)
{
   hp = hpd;
}
int Object::getHP()
{
   return hp;
}
void Object::SetPower(int powerh)
{
    power = powerh;
}
int Object::getPower()
{
    return power;
}
void Object::SetGravity(float g)
{
    gravity = g;
}
float Object::getGetGravity()
{
   return gravity;
}






void Object::Move(int where, int Npad)
{

/// if(offset.x <= 0 || offset.x >= lim_w || offset.y <= 0 || offset.y >= lim_h)
 {
	  switch(where)
	  {
		 case OBJECT_UP:
			 offset.y -= Npad;
			 break;
		 case OBJECT_DOWN:
			  offset.y +=Npad;
			 break;
		 case OBJECT_RIGHT:
			 offset.x += Npad;
		   break;
		 case OBJECT_LEFT:
			 offset.x -= Npad;
			break;
	  }
 }

}
