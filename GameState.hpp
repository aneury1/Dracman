/*
 * GameState.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#ifndef GAMESTATE_HPP_
#define GAMESTATE_HPP_


#include"Device.hpp"


class GameState
{
  public:
   GameState(){}
   virtual ~GameState(){};
   virtual void init(Device *device) = 0;
   virtual void Update(Device *device) = 0;
   virtual void Draw(Device *device) = 0;

};

bool checkCollision( SDL_Rect a, SDL_Rect b );


#endif /* GAMESTATE_HPP_ */
