/*
 * PlayState.cpp
 *
 *  Created on: 14 de ene. de 2016
 *      Author: aneury
 */


#include<SDL2/SDL.h>
#include<iostream>
#include<string>
using namespace std;

#include"Device.hpp"
#include"InputHandler.hpp"
#include"GraphicsManager.hpp"
#include"PlayState.hpp"
#include"mapa.hpp"

 void PlayState::init(Device *device)
 {
   GraphicsManager::Graphics()->LoadImg(device, "map.bmp", "map");
   GraphicsManager::Graphics()->LoadImg(device, "cofre.bmp", "cofre");
   GraphicsManager::Graphics()->LoadImg(device, "black.bmp", "black");
   GraphicsManager::Graphics()->LoadImg(device, "key.bmp", "key");
  // GraphicsManager::Graphics()->LoadImg(device, "player.bmp", "player");
   Map::Mapper()->loadTileMap(device,"test.txt","test");
   player = new Object(device,0,0,64,64,0,"key");

 }
 void PlayState::Update(Device *device)
 {


	 if(InputHandler::InputManager()->IsPressed(SDLK_RIGHT))
	 {
		 SDL_Log("right");
		 player->SetXPosition(player->getXposition() + 32);
	 }
	 else
	 if(InputHandler::InputManager()->IsPressed(SDLK_LEFT))
		 {
              SDL_Log("left");
			 player->SetXPosition(player->getXposition() - 32);
		 }
	 else if(InputHandler::InputManager()->IsPressed(SDLK_UP))
	 {
		 SDL_Log("up");
		 player->setYPosition(player->getYposition() - 32);

	 }
	 else if(InputHandler::InputManager()->IsPressed(SDLK_DOWN))
	 	 {
	 		 SDL_Log("up");
	 		 player->setYPosition(player->getYposition() +32);

	 	 }
 }
 void PlayState::Draw(Device *device)
 {
	 //SDL_SetRenderDrawColor(device->render, 0x0 ,0x0 ,0x0, 0xff);
	 Map::Mapper()->DrawMap(device,"test");
	 GraphicsManager::Graphics()->DrawImg(device,player->getId(),player->GetClipset(),player->GetOffset());

 }
