/*
 * Tile.hpp
 *
 *  Created on: 3 de ene. de 2016
 *      Author: aneury
 */

#ifndef TILE_HPP_
#define TILE_HPP_


#ifndef _ANDROID_DEV
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_image.h>



#else

#include<SDL.h>
#include<SDL_ttf.h>
#include<SDL_mixer.h>
#include<SDL_image.h>

#endif


#include<map>
#include<sstream>
#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;

#define TILE_SIZE 32

class Tile
{
   SDL_Rect offset;
   SDL_Rect clipset;
   char tiletype;


public:


   Tile(int x, int y, char type);
   SDL_Rect getOffset();
   SDL_Rect getClipset();
   int getX();
   int getY();
   int getTileSize();
   char getType();

};





#endif /* TILE_HPP_ */
