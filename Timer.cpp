/*
int fps_sincronizar (void)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	static int t;
	static int tmp;
	static int tl = 0;

	t = SDL_GetTicks ();

	if (t - tl >= FRECUENCIA)
	{
		tmp = (t - tl) / FRECUENCIA;
		tl += tmp * FRECUENCIA;
		return tmp;
	}
	else
	{
		SDL_Delay (FRECUENCIA - (t - tl));
		tl += FRECUENCIA;
		return 1;
	}
} * Timer.cpp
 *
 *  Created on: 2 de ene. de 2016
 *      Author: aneury
 */




#include"Timer.hpp"

Timer *Timer::m_instance = 0;

unsigned int  Timer::StartTime()
{
     this->startime = SDL_GetTicks();
     return startime;
}
unsigned int  Timer::CurrentTime()
{
    this->currentTime = this->startime - SDL_GetTicks();
    return currentTime;
}
unsigned int  Timer::FPS()
{
  return 60;
}


void Timer::Reset()
{
    //Initialize the variables
    mStartTicks = 0;
    mPausedTicks = 0;

    mPaused = false;
    mStarted = false;
}


void Timer::start()
{
    //Start the timer
    mStarted = true;

    //Unpause the timer
    mPaused = false;

    //Get the current clock time
    mStartTicks = SDL_GetTicks();
    mPausedTicks = 0;
}




void Timer::stop()
{
    //Stop the timer
    mStarted = false;

    //Unpause the timer
    mPaused = false;

    //Clear tick variables
    mStartTicks = 0;
    mPausedTicks = 0;
}

void Timer::pause()
{
    //If the timer is running and isn't already paused
    if( mStarted && !mPaused )
    {
        //Pause the timer
        mPaused = true;

        //Calculate the paused ticks
        mPausedTicks = SDL_GetTicks() - mStartTicks;
        mStartTicks = 0;
    }
}

void Timer::unpause()
{
    //If the timer is running and paused
    if( mStarted && mPaused )
    {
        //Unpause the timer
        mPaused = false;

        //Reset the starting ticks
        mStartTicks = SDL_GetTicks() - mPausedTicks;

        //Reset the paused ticks
        mPausedTicks = 0;
    }
}


Uint32 Timer::getTicks()
{
    //The actual timer time
    Uint32 time = 0;

    //If the timer is running
    if( mStarted )
    {
        //If the timer is paused
        if( mPaused )
        {
            //Return the number of ticks when the timer was paused
            time = mPausedTicks;
        }
        else
        {
            //Return the current time minus the start time
            time = SDL_GetTicks() - mStartTicks;
        }
    }

    return time;
}

bool Timer::isStarted()
{
    //Timer is running and paused or unpaused
    return mStarted;
}

bool Timer::isPaused()
{
    //Timer is running and paused
    return mPaused && mStarted;
}


int Timer::fps_sincronizar (void)
{
	SDL_Log(" -- %s --", __FUNCTION__);
	static int t;
	static int tmp;
	static int tl = 0;

	t = SDL_GetTicks ();

	if (t - tl >= FRECUENCIA)
	{
		tmp = (t - tl) / FRECUENCIA;
		tl += tmp * FRECUENCIA;
		return tmp;
	}
	else
	{
		SDL_Delay (FRECUENCIA - (t - tl));
		tl += FRECUENCIA;
		return 1;
	}
}


















