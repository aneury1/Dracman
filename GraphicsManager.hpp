/*
 * GraphicsManager.hpp
 *
 *  Created on: 29 de dic. de 2015
 *      Author: aneury
 */

#ifndef GRAPHICSMANAGER_HPP_DEFINED
#define GRAPHICSMANAGER_HPP_DEFINED


#include"Device.hpp"
#include"Object.hpp"










class GraphicsManager
{
		GraphicsManager(){}
		static GraphicsManager* m_instance;

	private:
      map<string, SDL_Texture*>m_img;
      map<string, TTF_Font*>m_font;
	public:
    static GraphicsManager *Graphics()
    {
    	if(m_instance == NULL)
    	{
    		m_instance = new GraphicsManager();
    	}
    	return m_instance;
    }

      void SetViewPort(Device *device, SDL_Rect rect);



       void LoadImg(Device *device, string src, string id);

       void DrawImg(Device *device, string id, int x,int y,int w,int h );
       void DrawImg(Device *device, string id, SDL_Rect clipset,SDL_Rect offset );
       void DrawImg(Device *device, string id,  int x, int y, int w, int h, int col, int row, int hsize, int vsize);
       void DrawImg(Device *device,string id, int x, int y,int w,int h, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip);
       void DrawImg(Device *device,string id, int x, int y, int w,int h,int col, int row,int hsize, int vsize, double angle, SDL_Point* center, SDL_RendererFlip flip);
       //void DrawImg(Device *device, Object object);

       void ModuleColorImage(Device *device, string id, SDL_Color color);
       void ModuleColorImage(Device *device, string id, Uint8 r,Uint8 g,Uint8 b );
       void setBlendMode( SDL_BlendMode blending,string id );
       void setAlpha( Uint8 alpha,string id );

       void DeleteImage(Device *device, string id);

       void LoadFont(Device *device, string src, int ptsize = 32);
       void CreateText(Device *device, string id, string text, SDL_Color color);
       void RenderText(Device *device, string text, int x, int y ,int w, int h);

       //void DrawText(Device *device , string text, int x, int y);
#ifdef TO_IMPLEMENT
       void DrawPoint(Device *device, int x,int y);
       void DrawLine(Device *device , int x1, int y1, int x2, int y2, SDL_Color color);
       void OpenGl(Device *device, void* (*pfi)())
#endif


};





#endif /* GRAPHICSMANAGER_HPP_ */
